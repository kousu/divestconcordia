DivestConcordia.ca
==================

This is the source for https://divestconcordia.ca.

It uses [Publii](https://getpublii.com), a GUI static site generator,
meant to make static generators accessible to non-techies.
To work with this repo, install and run Publii, then put (via symlink or moving)
this folder into ~/Publii/sites/, and/or edit your Publii settings to point it at this folder.


Publishing Rights
-----------------

Uploading is to ftp://ftp.sustainableconcordia.ca;
you can get an account from anghelos@sustainableconcordia.ca;
since this is shared hosting, be careful to set the upload paths correctly.

Important Details
-----------------

Publii is a bit fragile. It has an HTML editor, but it is pretty cavalier
about rewriting your HTML or even flat out deleting parts.
For example, if you put a `<p>` inside an `<a>` and then save,
it will drop the `<a>` entirely because it doesn't know how to handle that.
Keep obsessive backups. Keeping the project in git is good.

There is a "poster grid" style in use on the Materials and Research pages.
It displays a (unordered) list as a grid of 300x200px boxes.
It is designed to contain a link around either an image -- which should be
exactly 300x200px -- or a line of text; since text won't fill the whole area,
it is vertically and horizontally centered, but for the centering to work
**it must be wrapped in a `<p>` ("paragraph") tag**.

Publii's right/left aligned images are done using CSS floats.
You should insert `<p style="clear: both"></p>` at the end of any
section using them, to avoid such images spilling into the next.

-----

TODO:
- [ ] Merge my other work folder into here or here into my other work folder
  - that has private links tho? be careful!
- [ ] Tech
  - Mobile
   - [ ] Pure-css "hamburger" menu
    - using the "checkbox hack":
      - https://w3bits.com/css-responsive-nav-menu/
      - https://code-boxx.com/simple-responsive-pure-css-hamburger-menu/
  - [ ] Enable marginless display on pages
   - right now index.hbs is a special case to allow the hero-1 and hero-2 splashes to work
     but if you were to try to use them on any other page or post you would get white margins on the side
     can we add an extra toggle to each page in the theme config and use it to decide how to render?
  - [x] Put the font on the site itself / generally import all external resources needed instead of using CDNs
  - [ ] Centre the featured posts
  - [ ] Document how to load on another computer
  - [ ] considering migrating to a different host: one with better bandwidth and an sftp connection
  - [x] Migrate front page into a post
  - [ ] Figure out if we can use theme partials *inside* of posts; is there some way themes can add widgets to the site?
     - it would be nice to be able to have a post
      - another option *maybe* would be to build a partial
  - [ ] use <section> for the splash sections instead of just a div
  - [ ] Enable redirect-to-https
  - [x] svg-fix.js bug: https://github.com/GetPublii/Publii/issues/546
    - it turns out this is a workaround that only applies to the Preview sites (which *generate differently than the published sites*???) solely because svgs under file:// aren't allowed arbitrary file access; which is probably a good thing, but also dammit.
    - I patched the theme so it doesn't need jQuery, but it still needs some javascript. But that's only when viewed from file:// links.
  - Improve poster-grid layout
    - [ ] Explore how  https://cinemabeaubien.com/fr/a-laffiche does it
    - [ ] extra content in the poster-grid overflow their bounding boxes, because the outer and inner blocks are both *fixed* to 300x200px, so if you add extra content after the inner block it just runs off the edge and collides. deal with this.

- [ ] Accessibility
  - [ ] Test on various browsers
  - [ ] Test with/without adblockers
  - [ ] Test in lynx
  - [ ] Test with a screenreader
  - [ ] Test for red/green colourblindness
- Content
  - [ ] Why Page
     - merge old content from
       - https://web.archive.org/web/20150812121712/http://divestconcordia.org/why-divest/
       - https://web.archive.org/web/20170628005824/http://divestconcordia.org/why-divest/
  - [ ] News
    - Fill in a couple of starter blog posts
  - Media
    - [x] movement map
    - [x] video Emily sent: https://drive.google.com/file/d/1T2rZH0QAFfS8sQB9eN_z4hDWfP7hBfl5/view
      - [x] upload it to divestconcordia@gmail.com's Youtube account and embed from there (pro: less hosting cost; con: supporting google surveillance state)
    - [-] Recover pamphlet/poster PDFs from the old site
      - [-] make and attach 300x200px thumbnails of their front pages as their links on Materials
      - [ ] the pamphlet citation pages have a second link to their respective pamphlets

    - Citations
      - [x] Drop citations page; make sure posters have citations inline
      - [x] Include explicit weblinks to as many of our citations as possible:
        - [x] 2014-2015 Endowment Report: 
        - ...
      - [ ] Where possible, make web link citations into internetarchive links for linkrot protection
      - [ ] Find the citations for "Reliable Investment" (green pamphlet) and/or remove that page if it has no or uses inline citations
      - [ ] Front page:
        - [ ] find a citation for "supports the tar sands"
        - [ ] find a citation for "disproportionately affects"
  - [-] History section
    - [ ] Blurb (mine Markus's research paper for details)
    - [x] News ("In the press") links
      - [x] http://www.concordia.ca/cunews/main/stories/2016/02/09/concordias-sustainable-investment-initiative-the-next-stage.html
      - [x] https://montrealgazette.com/news/local-news/concordia-becomes-first-canadian-university-to-begin-divesting-from-fossil-fuels
      - [x] https://thelinknewspaper.ca/article/the-fight-for-fossil-fuel-divestment-rests-in-limbo-at-concordia
      - [ ] Give better link titles than just the URLs for these news links; use APA/MLA citation format for news articles.
  - [ ] Research
   - [-] Link to Concordia's foundation annual report (or a copy of?)
    - https://www.concordia.ca/content/dam/concordia/aar/docs/foundation/2017-18-Concordia-Foundation-Annual-Report.pdf
      - not including this one; it's cited by some of the research papers
    - [x] Erik's paper (convert to PDF first!)
   - [x] Post class research articles:
    - https://drive.google.com/drive/folders/1RnV8N_4YPbdoW3Fkunl4EWKnV9ZnZwrE/draft-binder.pdf
      - [x] needs splitting up

  - dead links:
    - the "What do the students think?" video references sustainableconcordia.ca; check this link is good and also consider if we want it there
    - the zine references bit.ly, not bitly.com -- bit.ly got killed!
      - umap has its own url shortener. get the zine authors to republish?
  - [x] Contact Us
  - [x] About Us
  - [ ] Fact check content
   - and update content when there are newer numbers available


Nick todo:
- Why? page
- hamburger menu
- wayback machine linkify; but do this after Emily fixes up all the citations.

Emily todo:
- load site onto your laptop
- write history section
- fill in a starter blog post
  - UQAM's divestment?
  - ?
- get pamphlets
  - upload them in input/media/files/materials/
  - make 300x200 thumbnails for them and add them to the list on Materials
- proofread Why? section
- proofread Materials page (there's redundant pamphlets and sections)
- fill in the missing citations on the front page, for the green pamphlet
- fact check everything


Embeds
======

Several pieces of content on the site are hosted externally.

- The "What do the students think?" video: https://www.youtube.com/watch?v=VoSSxGHkAbM
  - hosted under sustainableconcordia@gmail.com; the Divest comms coordinator has the password.
- The tar sands video: https://www.youtube.com/watch?v=dwwVo3CEFAQ
  - hosted by YouTube under a different account we don't control
- The canada-wide movement map: https://umap.openstreetmap.fr/en/map/university-fossil-fuel-divestment-groups-in-canada_355343
  - edit link: http://umap.openstreetmap.fr/en/map/anonymous-edit/355343:-6erE_gUkOUybc4dDKpNr6oMDrw
  - cloned from bitly.com/divestmap aka https://umap.openstreetmap.fr/en/map/university-fossil-fuel-divestment-groups-in-canada_308202#14/45.4952/-73.5785 because that has "everyone can edit" set and we want to avoid defacement
   


Tech Scraps
-----------  
  
  https://www.imagemagick.org/script/command-line-options.php#crop
  chttps://stackoverflow.com/questions/2322750/replace-transparency-in-png-images-with-white-background
  ```
  convert image.png -background white -alpha remove -alpha off white.png
  ```

pdf is a vector format
imagemagick works on raster formats
it can handle pdfs but it does it by *first* downconverting to bitmap
so:
  https://stackoverflow.com/questions/6605006/convert-pdf-to-image-with-high-resolution
  ```
  # the important thing is that -density to come before the pdf file, because that'll determine the resolution
  # it's in DPI; 300 is the conventional printing density.

  convert -density 300 file.pdf bitmap.png
  ```
  
   

the research papers all have a page number in their corner *from draft-binder.pdf* ughqc
   
   diff --git a/input/themes/divestconcordia/index.hbs b/input/themes/divestconcordia/index.hbs
index be3b284..408b5e1 100644
--- a/input/themes/divestconcordia/index.hbs
+++ b/input/themes/divestconcordia/index.hbs
@@ -36,7 +36,7 @@
       </h1>
     </div>
 
-    <div id="instititions-splash" class="parallax" style="background-image: url({{@website.assetsUrl}}/img/earth.jpg); text-align: right">
+    <div id="instititions-splash" class="parallax" style="background-image: url({{@website.assetsUrl}}/img/earth.jpg); text-align: center">
       <!-- two-column layout for this block; TODO: port this to use display: grid -->
       <div>
       <div style="display: inline-block; text-align: right; vertical-align: top">
@@ -57,8 +57,9 @@
     </div>
 
     <div id="industry-splash" class="parallax" style="background-image: url({{@website.assetsUrl}}/img/industry.jpg)"></div>
-
+    
             {{#if featuredPosts}}
+            <div class="container">
                 <div class="featured-posts">
                     <h3 class="box__title">Featured posts</h3>
                     {{#each featuredPosts}}
@@ -97,6 +98,7 @@
                         {{/checkIf}}
                     {{/each}}
                 </div>{{!-- .featured-posts --}}
+            </div>
             {{/if}}
 
     <div id="tarsands-splash" class="hero-1">
(END)


